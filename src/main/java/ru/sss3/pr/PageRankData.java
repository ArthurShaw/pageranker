package ru.sss3.pr;

public class PageRankData {

    private final String page;
    private final double rank;

    PageRankData(String page, double rank) {
        this.page = page;
        this.rank = rank;
    }

    public String page() {
        return page;
    }

    public double rank() {
        return rank;
    }

}
